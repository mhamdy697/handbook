---
title: "Monte Carlo Guide"
description: "Monte Carlo (MC) is our Data Observability tool and helps us deliver better results more efficiently."
---

## Overview

**Monte Carlo Guide** pages are available in our [Internal GitLab Handbook](https://internal.gitlab.com/handbook/enterprise-data/platform/monte-carlo).
