---

title: "CSM/CSE Webinar Calendar"
---
# On this page



View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---
# Upcoming Webinars

We’d like to invite you to our free upcoming webinars and labs in the month of January 2024.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

## January 2024

### AMER Time Zone Webinars & Labs

#### Intro to GitLab
##### January 9th, 2024 at 9:00-10:00AM Pacific Time / 1:00-2:00PM Eastern Time

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_J6u0UraeSESGFqjkqNyccg#/registration)

#### Hands-On GitLab CI Lab 
##### January 10th, 2023 at 9:00-10:30AM Pacific Time / 12:00-1:30PM Eastern Time

Join us for a hands-on GitLab CI lab and learn how it can fit in your organization!

Learn how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_u_CBtGNhQxOfHfCcu_HtrA#/registration)

#### Intro to CI/CD
##### January 16th, 2024 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_soWVm-TSQdOsqfBxI-s56g#/registration)

#### Hands-On GitLab CI Lab for Jenkins Users
##### January 17th, 2023 at 9:00-11:00AM Pacific Time / 12:00-2:00PM Eastern Time

Join us for a hands-on GitLab CI lab and learn how it can fit in your organization!

We will kick things off by going over the differences between CI/CD in Jenkins and GitLab, syntax requirements, advantages to using GitLab, and how you can achieve the same outcomes in GitLab. Getting started with CI/CD in GitLab will take a lot less time than tends to be required for Jenkins, and your users can stay in a single platform. 

We will then dive into how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_RIbcenwkSuaVtDcmsABbdQ#/registration)

#### GitLab Runner Fundamentals and What You Need to Know
##### January 19th, 2024 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Runners are a requirement for operating GitLab CI/CD. Learn about Runner architecture, options for deployments, supported operating systems, and optimizing them for most efficient usage. We will be discussing best practices, decisions that you need to make before deploying at scale, and options you have for monitoring fleets of runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_O_HsFdMBQ56aTOusd0_gaw#/registration)

#### Advanced CI/CD
##### January 23rd, 2024 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_iNqNs3ODTEiDvckmYQGClQ#/registration)

#### Hands-on Lab: Security and Compliance in GitLab
##### January 24th, 2023 at 9:00-11:00AM Pacific Time / 12:00-2:00PM Eastern Time

In this lab we will focus on how you can secure your application with GitLab. 

We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. 

Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_eDhmZ3uIR5uXWLkbDEnAzg#/registration)

#### Security and Compliance
##### January 26th, 2024 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_Zg7ms6dTQaKdT-zSRKfWLg#/registration)

### Jira to GitLab: Helping you transition to planning with GitLab
##### January 30th, 2024 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

GitLab offers robust team planning capabilities that work directly with the DevOps lifecycle. Keeping teams in a single application offers operational and financial advantages. Many customers inquire about what a transition would look like from Jira Software to GitLab to team planning. This webinar will start to outline the differences in functionality and advantages gained by using GitLab. 

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_zJJpJoOZTDqoAjeGpio_NQ#/registration)

#### AI in DevSecOps - GitLab Hands-On Lab
##### December 31st, 2023 at 9:00-10:30AM Pacific Time / 12:00-1:30PM Eastern Time

Join us for a hands-on GitLab AI lab where we will explore Artificial Intelligence and how it fits within the DevSecOps lifecycle. In this session, we will cover foundational implementation and use case scenarios such as Code Suggestions and Vulnerability Explanations.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN__-DANTdmTsikP6gKFNb7XQ#/registration)


### EMEA Time Zone Webinars & Labs

#### Intro to GitLab
##### January 9th, 2024 at 10:00-11:00AM UTC / 11:00AM-12:00PM CET

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_MDTjG0rJTF-rukCbPlqVDA#/registration)

#### Hands-On GitLab CI Lab 
##### DecembJanuary 10th, 2023 at 10:00-11:30AM UTC / 11:00AM-12:30PM CET

Join us for a hands-on GitLab CI lab and learn how it can fit in your organization!

Learn how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_c8z6pC12SK6TiZvIR-Nv3g#/registration)

#### Intro to CI/CD
##### January 16th, 2024 at 10:00-11:00AM UTC / 11:00AM-12:00PM CET

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_AYMxhTVTQ1KOdF3Oivx1aQ#/registration)

#### Hands-On GitLab CI Lab for Jenkins Users
##### January 17th, 2023 at 10:00AM-12:00PM UTC / 11:00AM-1:00PM CET

Join us for a hands-on GitLab CI lab and learn how it can fit in your organization!

We will kick things off by going over the differences between CI/CD in Jenkins and GitLab, syntax requirements, advantages to using GitLab, and how you can achieve the same outcomes in GitLab. Getting started with CI/CD in GitLab will take a lot less time than tends to be required for Jenkins, and your users can stay in a single platform. 

We will then dive into how to build simple GitLab pipelines and work up to more advanced pipeline structures and workflows, including security scanning and compliance enforcement.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_97qmaOkqSR6ZUPQK_JQ-ow#/registration)

#### GitLab Runner Fundamentals and What You Need to Know
##### January 19th, 2024 at 10:00-11:00AM UTC / 11:00AM-12:00PM CET

Runners are a requirement for operating GitLab CI/CD. Learn about Runner architecture, options for deployments, supported operating systems, and optimizing them for most efficient usage. We will be discussing best practices, decisions that you need to make before deploying at scale, and options you have for monitoring fleets of runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_Wq4ECs7XSRiuicFj9n5vYQ#/registration)

#### Advanced CI/CD
##### January 23rd, 2024 at 10:00-11:00AM UTC / 11:00AM-12:00PM CET

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_k7dyg0dmSXCR_d-VyStC_A#/registration)

#### Hands-on Lab: Security and Compliance in GitLab
##### January 24th, 2023 at 10:00-12:00AM UTC / 11:00AM-1:00PM CET

In this lab we will focus on how you can secure your application with GitLab. 

We will first take a look at how to apply scanners to your CICD pipelines in a hands-on exercise so that any vulnerabilities are caught as soon as the code is committed. 

Next, we will look at compliance frameworks and pipelines to show how you can ensure no one within your development teams is cutting corners and exposing your application.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_Eh04V1KVQbudD-yCFmmZ9Q#/registration)

#### Security and Compliance
##### January 26th, 2024 at 10:00-11:00AM UTC / 11:00AM-12:00PM CET

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_KpVa0DWpRdqUxGy311W8Eg#/registration)

### Jira to GitLab: Helping you transition to planning with GitLab
##### January 30th, 2024 at 10:00-11:00AM UTC / 11:00AM-12:00PM CET

GitLab offers robust team planning capabilities that work directly with the DevOps lifecycle. Keeping teams in a single application offers operational and financial advantages. Many customers inquire about what a transition would look like from Jira Software to GitLab to team planning. This webinar will start to outline the differences in functionality and advantages gained by using GitLab. 

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_1UOsVjFXQGGbYEGAFpiAWw#/registration)

#### AI in DevSecOps - GitLab Hands-On Lab
##### January 31st, 2023 at 10:00AM-11:30AM UTC / 11:00AM-12:30PM CET

Join us for a hands-on GitLab AI lab where we will explore Artificial Intelligence and how it fits within the DevSecOps lifecycle. In this session, we will cover foundational implementation and use case scenarios such as Code Suggestions and Vulnerability Explanations.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_f8cLG7PeQlyIhI8A18JHUg#/registration)

Check back later for more webinars & labs! 
